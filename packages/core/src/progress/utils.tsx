import React from 'react';
import Icon, { Type } from '../icon';

export const IconProgress = (props: { type: Type }) => <Icon type={props.type} />;
